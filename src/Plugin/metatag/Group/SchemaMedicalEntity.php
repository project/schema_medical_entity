<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'MedicalEntity' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_entity",
 *   label = @Translation("Schema.org: MedicalEntity"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalEntity",
 *   }),
 *   weight = 1000,
 * )
 */
class SchemaMedicalEntity extends SchemaGroupBase {

}
