<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'manufacturer' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_manufacturer",
 *   label = @Translation("manufacturer"),
 *   description = @Translation(""),
 *   name = "manufacturer",
 *   group = "schema_drug",
 *   weight = 190,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugManufacturer extends SchemaNameBase {

}
