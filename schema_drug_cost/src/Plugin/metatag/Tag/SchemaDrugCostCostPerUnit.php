<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'costPerUnit' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_cost_per_unit",
 *   label = @Translation("costPerUnit"),
 *   description = @Translation(""),
 *   name = "costPerUnit",
 *   group = "schema_drug_cost",
 *   weight = 60,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugCostCostPerUnit extends SchemaNameBase {

}
