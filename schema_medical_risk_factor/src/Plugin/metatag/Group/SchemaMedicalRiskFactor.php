<?php

namespace Drupal\schema_medical_risk_factor\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalRiskFactor' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_risk_factor",
 *   label = @Translation("Schema.org: MedicalRiskFactor"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalRiskFactor",
 *   }),
 *   weight = 2000,
 * )
 */
class SchemaMedicalRiskFactor extends SchemaMedicalEntity {

}
