<?php

namespace Drupal\schema_medical_risk_factor\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_risk_factor_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical risk_factor."),
 *   name = "@type",
 *   group = "schema_medical_risk_factor",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalRiskFactor",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalRiskFactorType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalRiskFactor',
    ];
  }

}
