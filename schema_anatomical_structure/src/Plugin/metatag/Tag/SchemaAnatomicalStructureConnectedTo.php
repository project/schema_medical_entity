<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'connectedTo' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_connected_to",
 *   label = @Translation("connectedTo"),
 *   description = @Translation("Other anatomical structures to which this structure is connected."),
 *   name = "connectedTo",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalStructureConnectedTo extends SchemaNameBase {

}
