<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'pathophysiology' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_pathophysiology",
 *   label = @Translation("pathophysiology"),
 *   description = @Translation("Changes in the normal mechanical, physical, and biochemical functions that are associated with this activity or condition."),
 *   name = "pathophysiology",
 *   group = "schema_medical_condition",
 *   weight = 80,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionPathophysiology extends SchemaNameBase {

}
