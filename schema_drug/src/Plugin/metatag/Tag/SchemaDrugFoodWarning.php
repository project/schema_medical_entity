<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'foodWarning' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_food_warning",
 *   label = @Translation("foodWarning"),
 *   description = @Translation(""),
 *   name = "foodWarning",
 *   group = "schema_drug",
 *   weight = 120,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugFoodWarning extends SchemaNameBase {

}
