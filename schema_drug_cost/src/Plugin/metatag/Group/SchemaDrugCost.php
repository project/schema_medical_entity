<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'DrugCost' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_drug_cost",
 *   label = @Translation("Schema.org: DrugCost"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/DrugCost",
 *   }),
 *   weight = 30,
 * )
 */
class SchemaDrugCost extends SchemaGroupBase {

}
