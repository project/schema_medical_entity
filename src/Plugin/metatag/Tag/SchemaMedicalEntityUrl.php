<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'url' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_url",
 *   label = @Translation("url"),
 *   description = @Translation("The URL of the medical entity."),
 *   name = "url",
 *   group = "schema_medical_entity",
 *   weight = 4,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityUrl extends SchemaNameBase {

}
