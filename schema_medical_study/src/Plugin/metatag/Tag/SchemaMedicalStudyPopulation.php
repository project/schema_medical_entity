<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'population' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_study_population",
 *   label = @Translation("population"),
 *   description = @Translation("Any characteristics of the population used in the study, e.g. 'males under 65'."),
 *   name = "population",
 *   group = "schema_medical_study",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalStudyPopulation extends SchemaNameBase {

}
