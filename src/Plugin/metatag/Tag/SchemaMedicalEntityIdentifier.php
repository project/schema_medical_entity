<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'identifier' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_identifier",
 *   label = @Translation("identifier"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. The identifier of the medical entity."),
 *   name = "identifier",
 *   group = "schema_medical_entity",
 *   weight = -6,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityIdentifier extends SchemaNameBase {

}
