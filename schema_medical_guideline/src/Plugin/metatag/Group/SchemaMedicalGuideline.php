<?php

namespace Drupal\schema_medical_guideline\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalGuideline' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_guideline",
 *   label = @Translation("Schema.org: MedicalGuideline"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalGuideline",
 *   }),
 *   weight = 1070,
 * )
 */
class SchemaMedicalGuideline extends SchemaMedicalEntity {

}
