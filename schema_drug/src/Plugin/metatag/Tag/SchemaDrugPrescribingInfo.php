<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'prescribingInfo' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_prescribing_info",
 *   label = @Translation("prescribingInfo"),
 *   description = @Translation(""),
 *   name = "prescribingInfo",
 *   group = "schema_drug",
 *   weight = 260,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugPrescribingInfo extends SchemaNameBase {

}
