<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'Drug' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_drug",
 *   label = @Translation("Schema.org: Drug"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/Drug",
 *   }),
 *   weight = 10,
 * )
 */
class SchemaDrug extends SchemaGroupBase {

}
