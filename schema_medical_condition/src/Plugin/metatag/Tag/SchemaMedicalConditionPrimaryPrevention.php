<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'primaryPrevention' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_primary_prevention",
 *   label = @Translation("primaryPrevention"),
 *   description = @Translation("A preventative therapy used to prevent an initial occurrence of the medical condition, such as vaccination."),
 *   name = "primaryPrevention",
 *   group = "schema_medical_condition",
 *   weight = 110,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionPrimaryPrevention extends SchemaNameBase {

}
