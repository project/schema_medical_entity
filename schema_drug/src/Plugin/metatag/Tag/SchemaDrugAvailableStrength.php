<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'availableStrength' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_available_strength",
 *   label = @Translation("availableStrength"),
 *   description = @Translation(""),
 *   name = "availableStrength",
 *   group = "schema_drug",
 *   weight = 50,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugAvailableStrength extends SchemaNameBase {

}
