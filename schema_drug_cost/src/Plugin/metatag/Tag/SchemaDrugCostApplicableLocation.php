<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'applicableLocation' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_applicable_location",
 *   label = @Translation("applicableLocation"),
 *   description = @Translation(""),
 *   name = "applicableLocation",
 *   group = "schema_drug_cost",
 *   weight = 20,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugCostApplicableLocation extends SchemaNameBase {

}
