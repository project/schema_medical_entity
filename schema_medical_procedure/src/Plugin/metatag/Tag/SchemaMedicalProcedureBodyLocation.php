<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'bodyLocation' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_body_location",
 *   label = @Translation("bodyLocation"),
 *   description = @Translation("Location in the body of the anatomical structure."),
 *   name = "bodyLocation",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedureBodyLocation extends SchemaNameBase {

}
