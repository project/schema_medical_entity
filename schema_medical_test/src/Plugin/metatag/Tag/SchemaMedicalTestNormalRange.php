<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'normalRange' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_test_normal_range",
 *   label = @Translation("normalRange"),
 *   description = @Translation("Range of acceptable values for a typical patient, when applicable."),
 *   name = "normalRange",
 *   group = "schema_medical_test",
 *   weight = 20,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalTestNormalRange extends SchemaNameBase {

}
