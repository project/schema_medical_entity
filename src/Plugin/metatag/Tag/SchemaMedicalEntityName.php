<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'name' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_name",
 *   label = @Translation("name"),
 *   description = @Translation("REQUIRED BY GOOGLE. The name of the medical entity."),
 *   name = "name",
 *   group = "schema_medical_entity",
 *   weight = -5,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityName extends SchemaNameBase {

}
