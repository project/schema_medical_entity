<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;
use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Schema.org MedicalEntity items should extend this class.
 */
class SchemaMedicalEntityBase extends SchemaNameBase {

  use SchemaMedicalEntityTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $element = []): array {

    $value = SchemaMetatagManager::unserialize($this->value());

    $input_values = [
      'title' => $this->label(),
      'description' => $this->description(),
      'value' => $value,
      '#required' => $element['#required'] ?? FALSE,
      'visibility_selector' => $this->visibilitySelector(),
    ];

    $form = $this->medicalEntityForm($input_values);

    if (empty($this->multiple())) {
      unset($form['pivot']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function testValue() {
    $items = [];
    $keys = self::medicalEntityFormKeys();
    foreach ($keys as $key) {
      switch ($key) {
        case '@type':
          $items[$key] = 'MedicalEntity';
          break;

        default:
          $items[$key] = parent::testDefaultValue(1, '');
          break;

      }
    }

    return $items;
  }

}
