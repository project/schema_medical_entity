<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'naturalProgression' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_natural_progression",
 *   label = @Translation("naturalProgression"),
 *   description = @Translation("The expected progression of the condition if it is not treated and allowed to progress naturally."),
 *   name = "naturalProgression",
 *   group = "schema_medical_condition",
 *   weight = 70,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionNaturalProgression extends SchemaNameBase {

}
