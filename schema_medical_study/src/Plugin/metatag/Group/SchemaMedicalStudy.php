<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalStudy' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_study",
 *   label = @Translation("Schema.org: MedicalStudy"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalStudy",
 *   }),
 *   weight = 2010,
 * )
 */
class SchemaMedicalStudy extends SchemaMedicalEntity {

}
