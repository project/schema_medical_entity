<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'associatedPathophysiology' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_system_associated_pathophysiology",
 *   label = @Translation("associatedPathophysiology"),
 *   description = @Translation("If applicable, a description of the pathophysiology associated with the anatomical system, including potential abnormal changes in the mechanical, physical, and biochemical functions of the system."),
 *   name = "associatedPathophysiology",
 *   group = "schema_anatomical_system",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalSystemAssociatedPathophysiology extends SchemaNameBase {

}
