<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_medical_entity\Plugin\metatag\Base\SchemaMedicalGuidelineBase;

/**
 * Provides a plugin for the 'guideline' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_guideline",
 *   label = @Translation("guideline"),
 *   description = @Translation("A medical guideline related to this entity."),
 *   name = "guideline",
 *   group = "schema_medical_entity",
 *   weight = 11,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityGuideline extends SchemaMedicalGuidelineBase {

}
