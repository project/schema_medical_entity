<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'procedureType' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_procedure_type",
 *   label = @Translation("procedureType"),
 *   description = @Translation("The type of procedure, for example Surgical, Noninvasive, or Percutaneous."),
 *   name = "procedureType",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedureProcedureType extends SchemaNameBase {

}
