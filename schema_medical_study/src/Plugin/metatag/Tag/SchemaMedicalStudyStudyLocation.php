<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'studyLocation' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_study_study_location",
 *   label = @Translation("studyLocation"),
 *   description = @Translation("The location in which the study is taking/took place."),
 *   name = "studyLocation",
 *   group = "schema_medical_study",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalStudyStudyLocation extends SchemaNameBase {

}
