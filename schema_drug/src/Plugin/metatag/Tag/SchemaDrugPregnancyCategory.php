<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'pregnancyCategory' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_pregnancy_category",
 *   label = @Translation("pregnancyCategory"),
 *   description = @Translation(""),
 *   name = "pregnancyCategory",
 *   group = "schema_drug",
 *   weight = 240,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugPregnancyCategory extends SchemaNameBase {

}
