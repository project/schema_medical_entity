<?php

namespace Drupal\schema_superficial_anatomy\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'SuperficialAnatomy' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_superficial_anatomy",
 *   label = @Translation("Schema.org: SuperficialAnatomy"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/SuperficialAnatomy",
 *   }),
 *   weight = 2040,
 * )
 */
class SchemaSuperficialAnatomy extends SchemaMedicalEntity {

}
