<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'expectedPrognosis' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_expected_prognosis",
 *   label = @Translation("expectedPrognosis"),
 *   description = @Translation("The likely outcome in either the short term or long term of the medical condition."),
 *   name = "expectedPrognosis",
 *   group = "schema_medical_condition",
 *   weight = 60,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionExpectedPrognosis extends SchemaNameBase {

}
