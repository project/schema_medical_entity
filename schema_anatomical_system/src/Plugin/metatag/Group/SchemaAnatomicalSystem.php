<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'AnatomicalSystem' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_anatomical_system",
 *   label = @Translation("Schema.org: AnatomicalSystem"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/AnatomicalSystem",
 *   }),
 *   weight = 1030,
 * )
 */
class SchemaAnatomicalSystem extends SchemaMedicalEntity {

}
