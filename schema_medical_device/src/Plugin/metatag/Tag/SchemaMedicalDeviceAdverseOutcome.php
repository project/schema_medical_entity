<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'adverseOutcome' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_adverse_outcome",
 *   label = @Translation("adverseOutcome"),
 *   description = @Translation("A possible complication and/or side effect of this therapy. If it is known that an adverse outcome is serious (resulting in death, disability, or permanent damage; requiring hospitalization; or is otherwise life-threatening or requires immediate medical attention), tag it as a seriouseAdverseOutcome instead."),
 *   name = "adverseOutcome",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDeviceAdverseOutcome extends SchemaNameBase {

}
