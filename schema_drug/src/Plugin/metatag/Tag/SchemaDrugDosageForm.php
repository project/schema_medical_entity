<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'dosageForm' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_dosage_form",
 *   label = @Translation("dosageForm"),
 *   description = @Translation(""),
 *   name = "dosageForm",
 *   group = "schema_drug",
 *   weight = 80,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugDosageForm extends SchemaNameBase {

}
