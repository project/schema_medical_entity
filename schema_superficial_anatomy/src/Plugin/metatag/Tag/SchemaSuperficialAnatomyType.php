<?php

namespace Drupal\schema_superficial_anatomy\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_superficial_anatomy_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of superficial anatomy."),
 *   name = "@type",
 *   group = "schema_superficial_anatomy",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "SuperficialAnatomy",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaSuperficialAnatomyType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'SuperficialAnatomy',
    ];
  }

}
