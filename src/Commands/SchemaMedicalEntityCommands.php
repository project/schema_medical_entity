<?php

namespace Drupal\schema_medical_entity\Commands;

use Symfony\Component\Serializer\Encoder\YamlEncoder;
use Drush\Commands\DrushCommands;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Generate tag plugin classes from schema config.
 *
 * @package Drupal\schema_medical_entity\Commands
 */
class SchemaMedicalEntityCommands extends DrushCommands {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * SchemaMedicalEntityCommands constructor.
   */
  public function __construct(FileSystemInterface $file_system, ModuleHandlerInterface $module_handler) {

    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;

  }

  /**
   * Generate tag plugin classes from schema config.
   *
   * @command schema_medical_entity:generate-tags
   *
   * @option module
   *   The module to source config/schema from.
   *
   * @option type
   *   The human-readable type name.
   *
   * @usage drush schema_medical_entity:generate-tags --module="schema_drug" --type="Drug"
   *   Generate tag plugin classes from schema config.
   *
   * @validate-module-enabled schema_medical_entity
   *
   * @aliases schema-medical-entity-generate-tags
   */
  public function generateTagPlugins($options = ['module' => '', 'type' => '']) {

    if (!$this->moduleHandler->moduleExists($options['module'])) {
      $this->writeln('Module "' . $options['module'] . '" doesn\'t exist.');
      return;
    }

    $stub_plugin = <<<PLUGIN
<?php

namespace Drupal\@module\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the '@label' meta tag.
 *
 * @MetatagTag(
 *   id = "@module_@property",
 *   label = @Translation("@label"),
 *   description = @Translation("@description"),
 *   name = "@label",
 *   group = "@module",
 *   weight = @weight,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,@annotation
 * )
 */
class @class extends SchemaNameBase {
  @methods
}

PLUGIN;

    $stub_annotation = <<<ANNOTATION
 *   property_type = "@type",
 *   tree_parent = {
 *     "@label",
 *   },
 *   tree_depth = 0,
ANNOTATION;

    $stub_methods = <<<METHODS
  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      '@label',
    ];
  }

METHODS;

    $module = $this->moduleHandler->getModule($options['module']);

    $identifier = 'metatag_tag.schema';
    $weight = 10;

    $source = implode(DIRECTORY_SEPARATOR, [
      $module->getPath(),
      InstallStorage::CONFIG_SCHEMA_DIRECTORY,
      "{$module->getName()}.$identifier.yml",
    ]);

    $target = "{$module->getPath()}/src/Plugin/metatag/Tag";
    $target_fs = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $target;

    $data = file_get_contents($source);
    $encoder = new YamlEncoder();
    $config = $encoder->decode($data, YamlEncoder::FORMAT);

    @$this->fileSystem->deleteRecursive($target_fs);

    if ($this->fileSystem->prepareDirectory($target_fs, FileSystemInterface::CREATE_DIRECTORY)) {

      foreach ($config as $property => $meta) {
        $property = substr($property, strlen("$identifier.{$module->getName()}_") + 1);
        $is_type = (stripos($meta['label'], '@') === 0);
        $uclabel = ucwords(ltrim($meta['label'], '@'));
        $description = ($is_type ? "REQUIRED. The type of {$options['type']}." : '');

        $parts = explode('_', $module->getName());
        array_walk($parts, function (&$part) {
          $part = ucwords($part);
        });
        $parts[] = $uclabel;
        $class = implode('', $parts);

        $current_annotation = PHP_EOL . str_replace(['@label', '@type'], [$options['type'], $property], $stub_annotation);
        $current_methods = PHP_EOL . str_replace(['@label', '@type'], [$options['type'], $property], $stub_methods);

        $search = [
          '@module',
          '@property',
          '@label',
          '@description',
          '@weight',
          '@class',
          '@annotation',
          '@methods',
        ];

        $replace = [
          $module->getName(),
          $property,
          $meta['label'],
          $description,
          $weight,
          $class,
        ];
        $replace[] = ($is_type ? $current_annotation : '');
        $replace[] = ($is_type ? $current_methods : '');

        $plugin = str_replace($search, $replace, $stub_plugin);

        file_put_contents("$target_fs/$class.php", $plugin);

        $this->writeln("Generated plugin $target/$class.php");

        $weight += 10;
      }

    }

  }

}
