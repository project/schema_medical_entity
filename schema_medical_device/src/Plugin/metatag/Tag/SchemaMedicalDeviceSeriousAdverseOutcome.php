<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'seriousAdverseOutcome' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_serious_adverse_outcome",
 *   label = @Translation("seriousAdverseOutcome"),
 *   description = @Translation("A possible serious complication and/or serious side effect of this therapy. Serious adverse outcomes include those that are life-threatening; result in death, disability, or permanent damage; require hospitalization or prolong existing hospitalization; cause congenital anomalies or birth defects; or jeopardize the patient and may require medical or surgical intervention to prevent one of the outcomes in this definition."),
 *   name = "seriousAdverseOutcome",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDeviceSeriousAdverseOutcome extends SchemaNameBase {

}
