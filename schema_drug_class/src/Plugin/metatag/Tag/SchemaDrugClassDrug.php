<?php

namespace Drupal\schema_drug_class\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'drug' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_class_drug",
 *   label = @Translation("drug"),
 *   description = @Translation(""),
 *   name = "drug",
 *   group = "schema_drug_class",
 *   weight = 20,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugClassDrug extends SchemaNameBase {

}
