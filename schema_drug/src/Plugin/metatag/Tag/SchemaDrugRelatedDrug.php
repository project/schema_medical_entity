<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedDrug' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_related_drug",
 *   label = @Translation("relatedDrug"),
 *   description = @Translation(""),
 *   name = "relatedDrug",
 *   group = "schema_drug",
 *   weight = 290,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugRelatedDrug extends SchemaNameBase {

}
