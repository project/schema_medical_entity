<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'healthCondition' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_study_health_condition",
 *   label = @Translation("healthCondition"),
 *   description = @Translation("Specifying the health condition(s) of a patient, medical study, or other target audience."),
 *   name = "healthCondition",
 *   group = "schema_medical_study",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalStudyHealthCondition extends SchemaNameBase {

}
