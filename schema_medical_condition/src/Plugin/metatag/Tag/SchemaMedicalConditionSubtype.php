<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'subtype' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_subtype",
 *   label = @Translation("subtype"),
 *   description = @Translation("A more specific type of the condition, where applicable, for example 'Type 1 Diabetes', 'Type 2 Diabetes', or 'Gestational Diabetes' for Diabetes."),
 *   name = "subtype",
 *   group = "schema_medical_condition",
 *   weight = 170,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionSubtype extends SchemaNameBase {

}
