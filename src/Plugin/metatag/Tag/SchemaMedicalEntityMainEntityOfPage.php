<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'mainEntityOfPage' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_main_entity_of_page",
 *   label = @Translation("mainEntityOfPage"),
 *   description = @Translation("Indicates a page (or other CreativeWork) for which this thing is the main entity being described."),
 *   name = "mainEntityOfPage",
 *   group = "schema_medical_entity",
 *   weight = 2,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "url",
 *   tree_parent = {},
 *   tree_depth = 0,
 * )
 */
class SchemaMedicalEntityMainEntityOfPage extends SchemaNameBase {

}
