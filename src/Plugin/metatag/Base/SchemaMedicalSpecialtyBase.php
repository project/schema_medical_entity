<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

/**
 * Schema.org MedicalSpecialty items should extend this class.
 */
class SchemaMedicalSpecialtyBase extends SchemaSupersededByBase {

  /**
   * {@inheritdoc}
   */
  public function supersededByForm($input_values) {

    $form = parent::supersededByForm($input_values);

    $form['@type']['#options'] = [
      'MedicalSpecialty' => $this->t('MedicalSpecialty'),
    ];

    return $form;
  }

}
