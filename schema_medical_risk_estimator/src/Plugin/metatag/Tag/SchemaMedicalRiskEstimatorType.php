<?php

namespace Drupal\schema_medical_risk_estimator\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_risk_estimator_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical risk_estimator."),
 *   name = "@type",
 *   group = "schema_medical_risk_estimator",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalRiskEstimator",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalRiskEstimatorType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalRiskEstimator',
      '- MedicalRiskCalculator',
      '- MedicalRiskScore',
    ];
  }

}
