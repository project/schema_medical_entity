<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'affectedBy' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_test_affected_by",
 *   label = @Translation("affectedBy"),
 *   description = @Translation("Drugs that affect the test's results."),
 *   name = "affectedBy",
 *   group = "schema_medical_test",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalTestAffectedBy extends SchemaNameBase {

}
