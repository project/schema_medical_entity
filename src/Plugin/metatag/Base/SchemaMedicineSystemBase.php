<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

/**
 * Schema.org MedicineSystem items should extend this class.
 */
class SchemaMedicineSystemBase extends SchemaSupersededByBase {

  /**
   * {@inheritdoc}
   */
  public function supersededByForm($input_values) {

    $form = parent::supersededByForm($input_values);

    $form['@type']['#options'] = [
      'MedicineSystem' => $this->t('MedicineSystem'),
    ];

    return $form;
  }

}
