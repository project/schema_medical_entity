<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'breastfeedingWarning' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_breastfeeding_warning",
 *   label = @Translation("breastfeedingWarning"),
 *   description = @Translation(""),
 *   name = "breastfeedingWarning",
 *   group = "schema_drug",
 *   weight = 60,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugBreastfeedingWarning extends SchemaNameBase {

}
