<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the '@type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of Drug."),
 *   name = "@type",
 *   group = "schema_drug",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "Drug",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaDrugType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'Drug',
    ];
  }

}
