<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'possibleComplication' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_possible_complication",
 *   label = @Translation("possibleComplication"),
 *   description = @Translation("A possible unexpected and unfavorable evolution of a medical condition. Complications may include worsening of the signs or symptoms of the disease, extension of the condition to other organ systems, etc."),
 *   name = "possibleComplication",
 *   group = "schema_medical_condition",
 *   weight = 90,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionPossibleComplication extends SchemaNameBase {

}
