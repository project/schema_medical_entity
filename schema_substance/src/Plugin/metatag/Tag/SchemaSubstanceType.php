<?php

namespace Drupal\schema_substance\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_substance_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical example."),
 *   name = "@type",
 *   group = "schema_substance",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "Substance",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaSubstanceType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'Substance',
      '- DietarySupplement',
      '- Drug',
    ];
  }

}
