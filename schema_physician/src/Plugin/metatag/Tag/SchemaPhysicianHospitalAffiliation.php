<?php

namespace Drupal\schema_physician\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_physician_hospital_affiliation' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_physician_hospital_affiliation",
 *   label = @Translation("hospitalAffiliation"),
 *   description = @Translation("Hospital affiliation for the physician. See <a href="":url"">Schema.org</a> for more information.", arguments = {
 *     ":url" = "https://schema.org/hospitalAffiliation",
 *   }),
 *   name = "hospitalAffiliation",
 *   group = "schema_physician",
 *   weight = 3,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "hospital_affiliation",
 *   tree_parent = {
 *     "Organization",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaPhysicianHospitalAffiliation extends SchemaNameBase {

}
