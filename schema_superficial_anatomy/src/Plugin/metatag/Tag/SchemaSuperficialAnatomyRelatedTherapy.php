<?php

namespace Drupal\schema_superficial_anatomy\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedTherapy' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_superficial_anatomy_related_therapy",
 *   label = @Translation("relatedTherapy"),
 *   description = @Translation("A medical therapy related to this anatomy."),
 *   name = "relatedTherapy",
 *   group = "schema_superficial_anatomy",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaSuperficialAnatomyRelatedTherapy extends SchemaNameBase {

}
