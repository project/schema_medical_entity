<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedCondition' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_system_related_condition",
 *   label = @Translation("relatedCondition"),
 *   description = @Translation("A medical condition associated with this anatomy."),
 *   name = "relatedCondition",
 *   group = "schema_anatomical_system",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalSystemRelatedCondition extends SchemaNameBase {

}
