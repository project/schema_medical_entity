<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'stage' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_stage",
 *   label = @Translation("stage"),
 *   description = @Translation("The stage of the condition, if applicable."),
 *   name = "stage",
 *   group = "schema_medical_condition",
 *   weight = 150,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionStage extends SchemaNameBase {

}
