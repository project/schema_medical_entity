<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'legalStatus' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_legal_status",
 *   label = @Translation("legalStatus"),
 *   description = @Translation(""),
 *   name = "legalStatus",
 *   group = "schema_drug",
 *   weight = 180,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugLegalStatus extends SchemaNameBase {

}
