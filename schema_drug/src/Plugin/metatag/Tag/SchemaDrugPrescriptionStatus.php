<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'prescriptionStatus' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_prescription_status",
 *   label = @Translation("prescriptionStatus"),
 *   description = @Translation(""),
 *   name = "prescriptionStatus",
 *   group = "schema_drug",
 *   weight = 270,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugPrescriptionStatus extends SchemaNameBase {

}
