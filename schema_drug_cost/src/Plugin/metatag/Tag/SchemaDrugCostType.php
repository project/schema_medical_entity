<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the '@type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of DrugCost."),
 *   name = "@type",
 *   group = "schema_drug_cost",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "DrugCost",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaDrugCostType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'DrugCost',
    ];
  }

}
