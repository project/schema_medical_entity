<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;
use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Schema.org MedicalCode items should extend this class.
 */
class SchemaMedicalCodeBase extends SchemaNameBase {

  use SchemaMedicalCodeTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $element = []): array {

    $value = SchemaMetatagManager::unserialize($this->value());

    $input_values = [
      'title' => $this->label(),
      'description' => $this->description(),
      'value' => $value,
      '#required' => $element['#required'] ?? FALSE,
      'visibility_selector' => $this->visibilitySelector(),
    ];

    $form = $this->medicalCodeForm($input_values);

    if (empty($this->multiple())) {
      unset($form['pivot']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function testValue() {
    $items = [];
    $keys = self::medicalCodeFormKeys();
    foreach ($keys as $key) {
      switch ($key) {
        case '@type':
          $items[$key] = 'MedicalCode';
          break;

        case 'codeValue':
          $items[$key] = 'M32.9';
          break;

        case 'codingSystem':
          $items[$key] = 'ICD-10';
          break;

        default:
          $items[$key] = parent::testDefaultValue(1, '');
          break;

      }
    }
    return $items;
  }

}
