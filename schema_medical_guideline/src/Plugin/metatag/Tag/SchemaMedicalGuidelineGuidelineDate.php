<?php

namespace Drupal\schema_medical_guideline\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'guidelineDate' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_guideline_guideline_date",
 *   label = @Translation("guidelineDate"),
 *   description = @Translation("Date on which this guideline's recommendation was made."),
 *   name = "guidelineDate",
 *   group = "schema_medical_guideline",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalGuidelineGuidelineDate extends SchemaNameBase {

}
