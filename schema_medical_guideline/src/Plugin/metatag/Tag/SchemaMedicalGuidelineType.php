<?php

namespace Drupal\schema_medical_guideline\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_guideline_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical guideline."),
 *   name = "@type",
 *   group = "schema_medical_guideline",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalGuideline",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalGuidelineType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalGuideline',
      '- MedicalGuidelineContraindication',
      '- MedicalGuidelineRecommendation',
    ];
  }

}
