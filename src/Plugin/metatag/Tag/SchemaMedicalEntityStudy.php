<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_medical_entity\Plugin\metatag\Base\SchemaMedicalStudyBase;

/**
 * Provides a plugin for the 'study' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_study",
 *   label = @Translation("study"),
 *   description = @Translation("A medical study or trial related to this entity."),
 *   name = "study",
 *   group = "schema_medical_entity",
 *   weight = 16,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityStudy extends SchemaMedicalStudyBase {

}
