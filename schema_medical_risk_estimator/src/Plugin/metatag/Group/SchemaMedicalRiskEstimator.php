<?php

namespace Drupal\schema_medical_risk_estimator\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalRiskEstimator' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_risk_estimator",
 *   label = @Translation("Schema.org: MedicalRiskEstimator"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalRiskEstimator",
 *   }),
 *   weight = 1090,
 * )
 */
class SchemaMedicalRiskEstimator extends SchemaMedicalEntity {

}
