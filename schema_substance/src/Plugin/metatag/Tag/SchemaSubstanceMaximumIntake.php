<?php

namespace Drupal\schema_substance\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'maximumIntake' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_substance_maximum_intake",
 *   label = @Translation("maximumIntake"),
 *   description = @Translation("Recommended intake of this supplement for a given population as defined by a specific recommending authority."),
 *   name = "maximumIntake",
 *   group = "schema_substance",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaSubstanceMaximumIntake extends SchemaNameBase {

}
