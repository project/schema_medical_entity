<?php

namespace Drupal\schema_medical_cause\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'causeOf' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_cause_cause_of",
 *   label = @Translation("causeOf"),
 *   description = @Translation("The condition, complication, symptom, sign, etc. caused."),
 *   name = "causeOf",
 *   group = "schema_medical_cause",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalCauseCauseOf extends SchemaNameBase {

}
