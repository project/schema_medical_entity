<?php

namespace Drupal\schema_drug_class\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the '@type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_class_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of DrugClass."),
 *   name = "@type",
 *   group = "schema_drug_class",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "DrugClass",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaDrugClassType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'DrugClass',
    ];
  }

}
