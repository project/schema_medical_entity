<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'AnatomicalStructure' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_anatomical_structure",
 *   label = @Translation("Schema.org: AnatomicalStructure"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/AnatomicalStructure",
 *   }),
 *   weight = 1020,
 * )
 */
class SchemaAnatomicalStructure extends SchemaMedicalEntity {

}
