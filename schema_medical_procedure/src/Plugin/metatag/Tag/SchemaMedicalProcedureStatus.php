<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'status' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_status",
 *   label = @Translation("status"),
 *   description = @Translation("The status of the study (enumerated)."),
 *   name = "status",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedureStatus extends SchemaNameBase {

}
