<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedTherapy' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_system_related_therapy",
 *   label = @Translation("relatedTherapy"),
 *   description = @Translation("A medical therapy related to this anatomy."),
 *   name = "relatedTherapy",
 *   group = "schema_anatomical_system",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalSystemRelatedTherapy extends SchemaNameBase {

}
