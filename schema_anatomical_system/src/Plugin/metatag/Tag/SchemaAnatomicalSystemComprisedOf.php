<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'comprisedOf' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_system_comprised_of",
 *   label = @Translation("comprisedOf"),
 *   description = @Translation("Specifying something physically contained by something else. Typically used here for the underlying anatomical structures, such as organs, that comprise the anatomical system."),
 *   name = "comprisedOf",
 *   group = "schema_anatomical_system",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalSystemComprisedOf extends SchemaNameBase {

}
