<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'labelDetails' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_label_details",
 *   label = @Translation("labelDetails"),
 *   description = @Translation(""),
 *   name = "labelDetails",
 *   group = "schema_drug",
 *   weight = 170,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugLabelDetails extends SchemaNameBase {

}
