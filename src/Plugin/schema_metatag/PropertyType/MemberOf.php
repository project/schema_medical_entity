<?php

namespace Drupal\schema_metatag\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * Provides a plugin for the 'memberOf' Schema.org property type.
 *
 * @SchemaPropertyType(
 *   id = "memberOf",
 *   label = @Translation("memberOf"),
 *   tree_parent = {
 *     "organization",
 *   },
 *   tree_depth = 0,
 *   property_type = "memberOf",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "name" = {
 *       "id" = "text",
 *       "label" = @Translation("name"),
 *       "description" = @Translation("The name of the work."),
 *     },
 *     "url" = {
 *       "id" = "url",
 *       "label" = @Translation("url"),
 *       "description" = @Translation("URL of place, organization."),
 *     },
 *   },
 * )
 */
class MemberOf extends PropertyTypeBase {

}
