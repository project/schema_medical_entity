<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'indication' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_indication",
 *   label = @Translation("indication"),
 *   description = @Translation("A factor that indicates use of this therapy for treatment and/or prevention of a condition, symptom, etc. For therapies such as drugs, indications can include both officially-approved indications as well as off-label uses. These can be distinguished by using the ApprovedIndication subtype of MedicalIndication."),
 *   name = "indication",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedureIndication extends SchemaNameBase {

}
