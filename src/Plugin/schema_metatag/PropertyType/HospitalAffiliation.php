<?php

namespace Drupal\schema_metatag\Plugin\schema_metatag\PropertyType;

use Drupal\schema_metatag\Plugin\schema_metatag\PropertyTypeBase;

/**
 * Provides a plugin for the 'hospital_affiliation' Schema.org property type.
 *
 * @SchemaPropertyType(
 *   id = "hospital_affiliation",
 *   label = @Translation("hospital_affiliation"),
 *   tree_parent = {
 *     "organization",
 *   },
 *   tree_depth = 0,
 *   property_type = "hospital_affiliation",
 *   sub_properties = {
 *     "@type" = {
 *       "id" = "type",
 *       "label" = @Translation("@type"),
 *       "description" = "",
 *     },
 *     "name" = {
 *       "id" = "text",
 *       "label" = @Translation("name"),
 *       "description" = @Translation("The name of the work."),
 *     },
 *     "url" = {
 *       "id" = "url",
 *       "label" = @Translation("url"),
 *       "description" = @Translation("URL of place, organization."),
 *     },
 *     "location" =  {
 *       "id" = "postal_address",
 *       "label" = @Translation("location"),
 *       "description" = @Translation("The location of the event."),
 *       "tree_parent" = {
 *         "PostalAddress",
 *       },
 *       "tree_depth" = 1,
 *     },
 *   },
 * )
 */
class HospitalAffiliation extends PropertyTypeBase {

}
