<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;
use Drupal\schema_medical_entity\Plugin\metatag\Base\SchemaMedicalEntityTrait;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical entity."),
 *   name = "@type",
 *   group = "schema_medical_entity",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalEntity",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalEntityType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return SchemaMedicalEntityTrait::medicalEntityTypes();
  }

}
