<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;

/**
 * Schema.org Image trait.
 */
trait SchemaMedicalEntityTrait {

  /**
   * Gets the list of MedicalEntity types.
   */
  public static function medicalEntityTypes() {
    return [
      'MedicalEntity',
      '- AnatomicalStructure',
      '- AnatomicalSystem',
      '- LifestyleModification',
      '- MedicalCause',
      '- MedicalCondition',
      '- MedicalContraindication',
      '- MedicalDevice',
      '- MedicalGuideline',
      '- MedicalIndication',
      '- MedicalIntangible',
      '- MedicalProcedure',
      '- MedicalRiskEstimator',
      '- MedicalRiskFactor',
      '- MedicalStudy',
      '- MedicalTest',
      '- Substance',
      '- SuperficialAnatomy',
    ];
  }

  /**
   * Form keys.
   */
  public static function medicalEntityFormKeys() {
    return [
      '@type',
      'code',
      'guideline',
      'legalStatus',
      'medicineSystem',
      'recognizingAuthority',
      'relevantSpecialty',
      'study',
    ];
  }

  /**
   * The form element.
   */
  public function medicalEntityForm($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $selector = ':input[name="' . $input_values['visibility_selector'] . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [
      $visibility['invisible'],
      $visibility2['invisible'],
    ];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    // Add a pivot option to the form.
    $form['pivot'] = (is_array($value) ? $this->pivotItem($value) : []);
    $form['pivot']['#states'] = $visibility;

    $options = array_combine(self::medicalEntityTypes(), self::medicalEntityTypes());

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => $options,
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $keys = static::medicalEntityFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key] = [
          '#type' => 'textfield',
          '#title' => $this->t('@key', [
            '@key' => $key,
          ]),
          '#default_value' => !empty($value[$key]) ? $value[$key] : '',
          '#maxlength' => 255,
          '#required' => $input_values['#required'],
          '#states' => $visibility,
        ];
      }
    }

    return $form;
  }

}
