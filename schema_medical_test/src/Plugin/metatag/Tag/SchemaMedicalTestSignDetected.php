<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'signDetected' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_test_sign_detected",
 *   label = @Translation("signDetected"),
 *   description = @Translation("A sign detected by the test."),
 *   name = "signDetected",
 *   group = "schema_medical_test",
 *   weight = 30,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalTestSignDetected extends SchemaNameBase {

}
