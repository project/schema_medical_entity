<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalTest' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_test",
 *   label = @Translation("Schema.org: MedicalTest"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalTest",
 *   }),
 *   weight = 2020,
 * )
 */
class SchemaMedicalTest extends SchemaMedicalEntity {

}
