<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'rxcui' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_rxcui",
 *   label = @Translation("rxcui"),
 *   description = @Translation(""),
 *   name = "rxcui",
 *   group = "schema_drug",
 *   weight = 300,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugRxcui extends SchemaNameBase {

}
