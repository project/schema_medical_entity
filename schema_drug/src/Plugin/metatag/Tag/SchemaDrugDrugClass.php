<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'drugClass' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_drug_class",
 *   label = @Translation("drugClass"),
 *   description = @Translation(""),
 *   name = "drugClass",
 *   group = "schema_drug",
 *   weight = 100,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugDrugClass extends SchemaNameBase {

}
