<?php

namespace Drupal\schema_medical_risk_estimator\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'includedRiskFactor' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_risk_estimator_included_risk_factor",
 *   label = @Translation("includedRiskFactor"),
 *   description = @Translation("A modifiable or non-modifiable risk factor included in the calculation, e.g. age, coexisting condition."),
 *   name = "includedRiskFactor",
 *   group = "schema_medical_risk_estimator",
 *   weight = 20,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalRiskEstimatorIncludedRiskFactor extends SchemaNameBase {

}
