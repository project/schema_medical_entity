<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'proprietaryName' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_proprietary_name",
 *   label = @Translation("proprietaryName"),
 *   description = @Translation(""),
 *   name = "proprietaryName",
 *   group = "schema_drug",
 *   weight = 280,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugProprietaryName extends SchemaNameBase {

}
