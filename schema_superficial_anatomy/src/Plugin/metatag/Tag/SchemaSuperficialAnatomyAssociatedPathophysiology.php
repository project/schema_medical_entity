<?php

namespace Drupal\schema_superficial_anatomy\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'associatedPathophysiology' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_superficial_anatomy_associated_pathophysiology",
 *   label = @Translation("associatedPathophysiology"),
 *   description = @Translation("If applicable, a description of the pathophysiology associated with the anatomical system, including potential abnormal changes in the mechanical, physical, and biochemical functions of the system."),
 *   name = "associatedPathophysiology",
 *   group = "schema_superficial_anatomy",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaSuperficialAnatomyAssociatedPathophysiology extends SchemaNameBase {

}
