<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'isProprietary' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_is_proprietary",
 *   label = @Translation("isProprietary"),
 *   description = @Translation(""),
 *   name = "isProprietary",
 *   group = "schema_drug",
 *   weight = 160,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugIsProprietary extends SchemaNameBase {

}
