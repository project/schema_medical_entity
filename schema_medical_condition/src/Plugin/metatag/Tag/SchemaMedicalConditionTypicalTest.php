<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'typicalTest' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_typical_test",
 *   label = @Translation("typicalTest"),
 *   description = @Translation("A medical test typically performed given this condition."),
 *   name = "typicalTest",
 *   group = "schema_medical_condition",
 *   weight = 180,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionTypicalTest extends SchemaNameBase {

}
