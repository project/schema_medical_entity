<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'alternateName' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_alternate_name",
 *   label = @Translation("alternateName"),
 *   description = @Translation("An alias for the item."),
 *   name = "alternateName",
 *   group = "schema_medical_entity",
 *   weight = 0,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityAlternateName extends SchemaNameBase {

}
