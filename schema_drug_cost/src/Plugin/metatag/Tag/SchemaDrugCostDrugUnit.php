<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'drugUnit' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_drug_unit",
 *   label = @Translation("drugUnit"),
 *   description = @Translation(""),
 *   name = "drugUnit",
 *   group = "schema_drug_cost",
 *   weight = 70,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugCostDrugUnit extends SchemaNameBase {

}
