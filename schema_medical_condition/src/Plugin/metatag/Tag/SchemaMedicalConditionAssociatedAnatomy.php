<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'associatedAnatomy' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_associated_anatomy",
 *   label = @Translation("associatedAnatomy"),
 *   description = @Translation("The anatomy of the underlying organ system or structures associated with this entity."),
 *   name = "associatedAnatomy",
 *   group = "schema_medical_condition",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionAssociatedAnatomy extends SchemaNameBase {

}
