<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'subjectOf' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_subject_of",
 *   label = @Translation("subjectOf"),
 *   description = @Translation("A CreativeWork or Event about this Thing."),
 *   name = "subjectOf",
 *   group = "schema_medical_entity",
 *   weight = 6,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "event",
 *   tree_parent = {},
 *   tree_depth = 0,
 * )
 */
class SchemaMedicalEntitySubjectOf extends SchemaNameBase {

}
