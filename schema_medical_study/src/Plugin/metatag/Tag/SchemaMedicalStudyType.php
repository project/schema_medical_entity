<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_study_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical study."),
 *   name = "@type",
 *   group = "schema_medical_study",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalStudy",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalStudyType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalStudy',
      '- MedicalObservationalStudy',
      '- MedicalTrial',
    ];
  }

}
