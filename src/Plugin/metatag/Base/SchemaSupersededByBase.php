<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;
use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Schema.org SupersededBy items should extend this class.
 */
class SchemaSupersededByBase extends SchemaNameBase {

  use SchemaSupersededByTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $element = []): array {

    $value = SchemaMetatagManager::unserialize($this->value());

    $input_values = [
      'title' => $this->label(),
      'description' => $this->description(),
      'value' => $value,
      '#required' => $element['#required'] ?? FALSE,
      'visibility_selector' => $this->visibilitySelector(),
    ];

    $form = $this->supersededByForm($input_values);

    if (empty($this->multiple())) {
      unset($form['pivot']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function testValue() {
    $items = [];
    $keys = self::supersededByFormKeys();
    foreach ($keys as $key) {
      switch ($key) {
        case '@type':
          $items[$key] = 'Class';
          break;

        case 'supersededBy':
          $items[$key] = 'something';
          break;

        default:
          $items[$key] = parent::testDefaultValue(1, '');
          break;

      }
    }
    return $items;
  }

}
