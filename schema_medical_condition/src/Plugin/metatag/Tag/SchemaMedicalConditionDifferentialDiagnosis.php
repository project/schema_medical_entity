<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'differentialDiagnosis' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_differential_diagnosis",
 *   label = @Translation("differentialDiagnosis"),
 *   description = @Translation("One of a set of differential diagnoses for the condition. Specifically, a closely-related or competing diagnosis typically considered later in the cognitive process whereby this medical condition is distinguished from others most likely responsible for a similar collection of signs and symptoms to reach the most parsimonious diagnosis or diagnoses in a patient."),
 *   name = "differentialDiagnosis",
 *   group = "schema_medical_condition",
 *   weight = 30,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionDifferentialDiagnosis extends SchemaNameBase {

}
