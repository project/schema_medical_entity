<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of anatomical structure."),
 *   name = "@type",
 *   group = "schema_anatomical_structure",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "AnatomicalStructure",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaAnatomicalStructureType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'AnatomicalStructure',
      '- Bone',
      '- BrainStructure',
      '- Joint',
      '- Ligament',
      '- Muscle',
      '- Nerve',
      '- Vessel',
    ];
  }

}
