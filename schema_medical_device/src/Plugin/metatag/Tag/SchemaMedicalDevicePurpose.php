<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'purpose' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_purpose",
 *   label = @Translation("purpose"),
 *   description = @Translation("A goal towards an action is taken. Can be concrete or abstract."),
 *   name = "purpose",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDevicePurpose extends SchemaNameBase {

}
