<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'bodyLocation' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_body_location",
 *   label = @Translation("bodyLocation"),
 *   description = @Translation("Location in the body of the anatomical structure."),
 *   name = "bodyLocation",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalStructureBodyLocation extends SchemaNameBase {

}
