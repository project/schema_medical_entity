<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'disambiguatingDescription' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_disambiguating_description",
 *   label = @Translation("disambiguatingDescription"),
 *   description = @Translation("A sub property of description. A short description of the item used to disambiguate from other, similar items."),
 *   name = "disambiguatingDescription",
 *   group = "schema_medical_entity",
 *   weight = 2,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityDisambiguatingDescription extends SchemaNameBase {

}
