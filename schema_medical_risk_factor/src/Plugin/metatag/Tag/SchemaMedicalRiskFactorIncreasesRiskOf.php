<?php

namespace Drupal\schema_medical_risk_factor\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'increasesRiskOf' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_risk_factor_increases_risk_of",
 *   label = @Translation("increasesRiskOf"),
 *   description = @Translation("The condition, complication, etc. influenced by this factor."),
 *   name = "increasesRiskOf",
 *   group = "schema_medical_risk_factor",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalRiskFactorIncreasesRiskOf extends SchemaNameBase {

}
