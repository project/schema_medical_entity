<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'activeIngredient' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_active_ingredient",
 *   label = @Translation("activeIngredient"),
 *   description = @Translation(""),
 *   name = "activeIngredient",
 *   group = "schema_drug",
 *   weight = 20,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugActiveIngredient extends SchemaNameBase {

}
