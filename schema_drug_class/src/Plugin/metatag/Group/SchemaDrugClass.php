<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'DrugClass' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_drug_class",
 *   label = @Translation("Schema.org: DrugClass"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/DrugClass",
 *   }),
 *   weight = 20,
 * )
 */
class SchemaDrugClass extends SchemaGroupBase {

}
