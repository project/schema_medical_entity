<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'partOfSystem' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_part_of_system",
 *   label = @Translation("partOfSystem"),
 *   description = @Translation("The anatomical or organ system that this structure is part of."),
 *   name = "partOfSystem",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalStructurePartOfSystem extends SchemaNameBase {

}
