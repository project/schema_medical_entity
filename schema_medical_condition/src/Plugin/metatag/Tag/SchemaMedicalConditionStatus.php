<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'status' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_status",
 *   label = @Translation("status"),
 *   description = @Translation("The status of the study (enumerated)."),
 *   name = "status",
 *   group = "schema_medical_condition",
 *   weight = 160,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionStatus extends SchemaNameBase {

}
