<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'outcome' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_study_outcome",
 *   label = @Translation("outcome"),
 *   description = @Translation("Expected or actual outcomes of the study."),
 *   name = "outcome",
 *   group = "schema_medical_study",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalStudyOutcome extends SchemaNameBase {

}
