<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;
use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Schema.org MedicalStudy items should extend this class.
 */
class SchemaMedicalStudyBase extends SchemaNameBase {

  use SchemaMedicalStudyTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $element = []): array {

    $value = SchemaMetatagManager::unserialize($this->value());

    $input_values = [
      'title' => $this->label(),
      'description' => $this->description(),
      'value' => $value,
      '#required' => $element['#required'] ?? FALSE,
      'visibility_selector' => $this->visibilitySelector(),
    ];

    $form = $this->medicalStudyForm($input_values);

    if (empty($this->multiple())) {
      unset($form['pivot']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function testValue() {
    $items = [];
    $keys = self::medicalStudyFormKeys();
    foreach ($keys as $key) {
      switch ($key) {
        case '@type':
          $items[$key] = 'MedicalStudy';
          break;

        case 'healthCondition':
        case 'outcome':
        case 'population':
        case 'sponsor':
        case 'status':
        case 'studyLocation':
        case 'studySubject':
        default:
          $items[$key] = parent::testDefaultValue(1, '');
          break;

      }
    }
    return $items;
  }

}
