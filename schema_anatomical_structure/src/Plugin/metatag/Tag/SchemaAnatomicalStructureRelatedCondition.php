<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedCondition' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_related_condition",
 *   label = @Translation("relatedCondition"),
 *   description = @Translation("A medical condition associated with this anatomy."),
 *   name = "relatedCondition",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalStructureRelatedCondition extends SchemaNameBase {

}
