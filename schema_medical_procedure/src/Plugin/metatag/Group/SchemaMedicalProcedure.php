<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalProcedure' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_procedure",
 *   label = @Translation("Schema.org: MedicalProcedure"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalProcedure",
 *   }),
 *   weight = 1080,
 * )
 */
class SchemaMedicalProcedure extends SchemaMedicalEntity {

}
