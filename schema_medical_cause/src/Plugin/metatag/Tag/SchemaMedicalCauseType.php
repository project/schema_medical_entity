<?php

namespace Drupal\schema_medical_cause\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_cause_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical cause."),
 *   name = "@type",
 *   group = "schema_medical_cause",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalCause",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalCauseType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalCause',
    ];
  }

}
