<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical condition."),
 *   name = "@type",
 *   group = "schema_medical_condition",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalCondition",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalConditionType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalCondition',
      '- InfectiousDisease',
      '- MedicalSignOrSymptom',
    ];
  }

}
