<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'costOrigin' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_cost_origin",
 *   label = @Translation("costOrigin"),
 *   description = @Translation(""),
 *   name = "costOrigin",
 *   group = "schema_drug_cost",
 *   weight = 50,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugCostCostOrigin extends SchemaNameBase {

}
