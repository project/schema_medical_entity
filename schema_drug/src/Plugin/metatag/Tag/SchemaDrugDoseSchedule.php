<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'doseSchedule' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_dose_schedule",
 *   label = @Translation("doseSchedule"),
 *   description = @Translation(""),
 *   name = "doseSchedule",
 *   group = "schema_drug",
 *   weight = 90,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugDoseSchedule extends SchemaNameBase {

}
