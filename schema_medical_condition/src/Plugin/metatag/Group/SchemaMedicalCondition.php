<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalCondition' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_condition",
 *   label = @Translation("Schema.org: MedicalCondition"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalCondition",
 *   }),
 *   weight = 1050,
 * )
 */
class SchemaMedicalCondition extends SchemaMedicalEntity {

}
