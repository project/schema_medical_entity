<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'nonProprietaryName' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_non_proprietary_name",
 *   label = @Translation("nonProprietaryName"),
 *   description = @Translation(""),
 *   name = "nonProprietaryName",
 *   group = "schema_drug",
 *   weight = 220,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugNonProprietaryName extends SchemaNameBase {

}
