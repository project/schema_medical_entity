<?php

namespace Drupal\schema_medical_risk_estimator\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'estimatesRiskOf' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_risk_estimator_estimates_risk_of",
 *   label = @Translation("estimatesRiskOf"),
 *   description = @Translation("The condition, complication, or symptom whose risk is being estimated."),
 *   name = "estimatesRiskOf",
 *   group = "schema_medical_risk_estimator",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalRiskEstimatorEstimatesRiskOf extends SchemaNameBase {

}
