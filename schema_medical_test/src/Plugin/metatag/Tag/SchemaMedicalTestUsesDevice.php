<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'usesDevice' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_test_uses_device",
 *   label = @Translation("usesDevice"),
 *   description = @Translation("Device used to perform the test."),
 *   name = "usesDevice",
 *   group = "schema_medical_test",
 *   weight = 50,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalTestUsesDevice extends SchemaNameBase {

}
