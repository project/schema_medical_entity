<?php

namespace Drupal\schema_medical_cause\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalCause' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_cause",
 *   label = @Translation("Schema.org: MedicalCause"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalCause",
 *   }),
 *   weight = 1040,
 * )
 */
class SchemaMedicalCause extends SchemaMedicalEntity {

}
