<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;

/**
 * Schema.org MedicalCode trait.
 */
trait SchemaMedicalCodeTrait {

  /**
   * Form keys.
   */
  public static function medicalCodeFormKeys() {
    return [
      '@type',
      'codeValue',
      'codingSystem',
    ];
  }

  /**
   * The form element.
   */
  public function medicalCodeForm($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $selector = ':input[name="' . $input_values['visibility_selector'] . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [
      $visibility['invisible'],
      $visibility2['invisible'],
    ];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    // Add a pivot option to the form.
    $form['pivot'] = (is_array($value) ? $this->pivotItem($value) : []);
    $form['pivot']['#states'] = $visibility;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'MedicalCode' => $this->t('MedicalCode'),
      ],
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $form['codeValue'] = [
      '#type' => 'textfield',
      '#title' => $this->t('codeValue'),
      '#description' => $this->t("A short textual code that uniquely identifies the value."),
      '#default_value' => !empty($value['codeValue']) ? $value['codeValue'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['codingSystem'] = [
      '#type' => 'textfield',
      '#title' => $this->t('codingSystem'),
      '#description' => $this->t("The coding system, e.g. 'ICD-10'."),
      '#default_value' => !empty($value['codingSystem']) ? $value['codingSystem'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $keys = static::medicalCodeFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key]['#states'] = $visibility;
      }
    }

    return $form;
  }

}
