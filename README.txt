
Schema.org MedicalEntity
--------------------------------------------------------------------------------
The module creates the following Schema.org object types:

Schema.org/MedicalEntity
Schema.org/AnatomicalStructure
Schema.org/AnatomicalSystem
Schema.org/LifestyleModification
Schema.org/MedicalCause
Schema.org/MedicalCondition
Schema.org/MedicalContraindication
Schema.org/MedicalDevice
Schema.org/MedicalGuideline
Schema.org/MedicalIndication
Schema.org/MedicalIntangible
Schema.org/MedicalProcedure
Schema.org/MedicalRiskEstimator
Schema.org/MedicalRiskFactor
Schema.org/MedicalStudy
Schema.org/MedicalTest
Schema.org/Substance
Schema.org/SuperficialAnatomy

Requirements
--------------------------------------------------------------------------------
The 3.0 version of the Schema.org Metatag module is required:
https://www.drupal.org/project/schema_metatag
