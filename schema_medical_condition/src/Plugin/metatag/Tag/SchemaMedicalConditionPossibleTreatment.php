<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'possibleTreatment' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_possible_treatment",
 *   label = @Translation("possibleTreatment"),
 *   description = @Translation("A possible treatment to address this condition, sign or symptom."),
 *   name = "possibleTreatment",
 *   group = "schema_medical_condition",
 *   weight = 100,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionPossibleTreatment extends SchemaNameBase {

}
