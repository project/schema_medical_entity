<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical device."),
 *   name = "@type",
 *   group = "schema_medical_device",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalDevice",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalDeviceType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalDevice',
    ];
  }

}
