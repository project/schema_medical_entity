<?php

namespace Drupal\schema_medical_guideline\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'guidelineSubject' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_guideline_guideline_subject",
 *   label = @Translation("guidelineSubject"),
 *   description = @Translation("The medical conditions, treatments, etc. that are the subject of the guideline."),
 *   name = "guidelineSubject",
 *   group = "schema_medical_guideline",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalGuidelineGuidelineSubject extends SchemaNameBase {

}
