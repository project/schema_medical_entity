<?php

namespace Drupal\schema_physician\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_physician_medical_specialty' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_physician_medical_specialty",
 *   label = @Translation("MedicalSpecialty"),
 *   description = @Translation("The speciality of the physician."),
 *   name = "MedicalSpecialty",
 *   group = "schema_physician",
 *   weight = 1.2,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaPhysicianMedicalSpecialty extends SchemaNameBase {

}
