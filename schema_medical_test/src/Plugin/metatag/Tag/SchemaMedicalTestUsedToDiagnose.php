<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'usedToDiagnose' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_test_used_to_diagnose",
 *   label = @Translation("usedToDiagnose"),
 *   description = @Translation("A condition the test is used to diagnose."),
 *   name = "usedToDiagnose",
 *   group = "schema_medical_test",
 *   weight = 40,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalTestUsedToDiagnose extends SchemaNameBase {

}
