<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'subStructure' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_sub_structure",
 *   label = @Translation("subStructure"),
 *   description = @Translation("Component (sub-)structure(s) that comprise this anatomical structure."),
 *   name = "subStructure",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalStructureSubStructure extends SchemaNameBase {

}
