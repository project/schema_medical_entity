<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'procedure' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_procedure",
 *   label = @Translation("procedure"),
 *   description = @Translation("A description of the procedure involved in setting up, using, and/or installing the device."),
 *   name = "procedure",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDeviceProcedure extends SchemaNameBase {

}
