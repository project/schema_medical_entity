<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'alcoholWarning' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_alcohol_warning",
 *   label = @Translation("alcoholWarning"),
 *   description = @Translation(""),
 *   name = "alcoholWarning",
 *   group = "schema_drug",
 *   weight = 40,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugAlcoholWarning extends SchemaNameBase {

}
