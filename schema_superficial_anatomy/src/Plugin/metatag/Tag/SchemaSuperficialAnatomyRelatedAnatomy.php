<?php

namespace Drupal\schema_superficial_anatomy\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedAnatomy' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_superficial_anatomy_related_anatomy",
 *   label = @Translation("relatedAnatomy"),
 *   description = @Translation("Anatomical systems or structures that relate to the superficial anatomy."),
 *   name = "relatedAnatomy",
 *   group = "schema_superficial_anatomy",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaSuperficialAnatomyRelatedAnatomy extends SchemaNameBase {

}
