<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;

/**
 * Schema.org MedicalStudy trait.
 */
trait SchemaMedicalStudyTrait {

  /**
   * Form keys.
   */
  public static function medicalStudyFormKeys() {
    return [
      '@type',
      'healthCondition',
      'outcome',
      'population',
      'sponsor',
      'status',
      'studyLocation',
      'studySubject',
    ];
  }

  /**
   * The form element.
   */
  public function medicalStudyForm($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $selector = ':input[name="' . $input_values['visibility_selector'] . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [
      $visibility['invisible'],
      $visibility2['invisible'],
    ];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    // Add a pivot option to the form.
    $form['pivot'] = (is_array($value) ? $this->pivotItem($value) : []);
    $form['pivot']['#states'] = $visibility;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'MedicalStudy' => $this->t('MedicalStudy'),
      ],
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $form['healthCondition'] = [
      '#type' => 'textfield',
      '#title' => $this->t('healthCondition'),
      '#description' => $this->t("Specifying the health condition(s) of a patient, medical study, or other target audience."),
      '#default_value' => !empty($value['healthCondition']) ? $value['healthCondition'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['outcome'] = [
      '#type' => 'textfield',
      '#title' => $this->t('outcome'),
      '#description' => $this->t("Expected or actual outcomes of the study."),
      '#default_value' => !empty($value['outcome']) ? $value['outcome'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['population'] = [
      '#type' => 'textfield',
      '#title' => $this->t('population'),
      '#description' => $this->t("Any characteristics of the population used in the study, e.g. 'males under 65'."),
      '#default_value' => !empty($value['population']) ? $value['population'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['sponsor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('sponsor'),
      '#description' => $this->t("A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event."),
      '#default_value' => !empty($value['sponsor']) ? $value['sponsor'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['status'] = [
      '#type' => 'textfield',
      '#title' => $this->t('status'),
      '#description' => $this->t("The status of the study (enumerated)."),
      '#default_value' => !empty($value['status']) ? $value['status'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['studyLocation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('studyLocation'),
      '#description' => $this->t("The location in which the study is taking/took place."),
      '#default_value' => !empty($value['studyLocation']) ? $value['studyLocation'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['studySubject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('studySubject'),
      '#description' => $this->t("The location in which the study is taking/took place."),
      '#default_value' => !empty($value['studySubject']) ? $value['studySubject'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $keys = static::medicalStudyFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key]['#states'] = $visibility;
      }
    }

    return $form;
  }

}
