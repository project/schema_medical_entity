<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'drug' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_drug",
 *   label = @Translation("drug"),
 *   description = @Translation("Specifying a drug or medicine used in a medication procedure."),
 *   name = "drug",
 *   group = "schema_medical_condition",
 *   weight = 40,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionDrug extends SchemaNameBase {

}
