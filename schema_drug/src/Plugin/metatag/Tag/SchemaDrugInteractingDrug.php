<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'interactingDrug' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_interacting_drug",
 *   label = @Translation("interactingDrug"),
 *   description = @Translation(""),
 *   name = "interactingDrug",
 *   group = "schema_drug",
 *   weight = 140,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugInteractingDrug extends SchemaNameBase {

}
