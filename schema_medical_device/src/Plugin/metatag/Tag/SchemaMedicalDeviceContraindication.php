<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'contraindication' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_contraindication",
 *   label = @Translation("contraindication"),
 *   description = @Translation("A contraindication for this therapy."),
 *   name = "contraindication",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDeviceContraindication extends SchemaNameBase {

}
