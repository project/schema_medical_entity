<?php

namespace Drupal\schema_substance\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'activeIngredient' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_substance_active_ingredient",
 *   label = @Translation("activeIngredient"),
 *   description = @Translation("An active ingredient, typically chemical compounds and/or biologic substances."),
 *   name = "activeIngredient",
 *   group = "schema_substance",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaSubstanceActiveIngredient extends SchemaNameBase {

}
