<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'clinicalPharmacology' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_clinical_pharmacology",
 *   label = @Translation("clinicalPharmacology"),
 *   description = @Translation(""),
 *   name = "clinicalPharmacology",
 *   group = "schema_drug",
 *   weight = 70,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugClinicalPharmacology extends SchemaNameBase {

}
