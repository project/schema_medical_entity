<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'epidemiology' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_epidemiology",
 *   label = @Translation("epidemiology"),
 *   description = @Translation("The characteristics of associated patients, such as age, gender, race etc."),
 *   name = "epidemiology",
 *   group = "schema_medical_condition",
 *   weight = 50,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionEpidemiology extends SchemaNameBase {

}
