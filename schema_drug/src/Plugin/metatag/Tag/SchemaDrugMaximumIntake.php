<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'maximumIntake' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_maximum_intake",
 *   label = @Translation("maximumIntake"),
 *   description = @Translation(""),
 *   name = "maximumIntake",
 *   group = "schema_drug",
 *   weight = 200,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugMaximumIntake extends SchemaNameBase {

}
