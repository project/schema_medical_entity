<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'costCategory' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_cost_category",
 *   label = @Translation("costCategory"),
 *   description = @Translation(""),
 *   name = "costCategory",
 *   group = "schema_drug_cost",
 *   weight = 30,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugCostCostCategory extends SchemaNameBase {

}
