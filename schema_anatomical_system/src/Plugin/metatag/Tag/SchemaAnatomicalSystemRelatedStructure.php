<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'relatedStructure' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_system_related_structure",
 *   label = @Translation("relatedStructure"),
 *   description = @Translation("Related anatomical structure(s) that are not part of the system but relate or connect to it, such as vascular bundles associated with an organ system."),
 *   name = "relatedStructure",
 *   group = "schema_anatomical_system",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalSystemRelatedStructure extends SchemaNameBase {

}
