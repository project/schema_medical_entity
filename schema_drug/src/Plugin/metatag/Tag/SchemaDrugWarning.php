<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'warning' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_warning",
 *   label = @Translation("warning"),
 *   description = @Translation(""),
 *   name = "warning",
 *   group = "schema_drug",
 *   weight = 310,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugWarning extends SchemaNameBase {

}
