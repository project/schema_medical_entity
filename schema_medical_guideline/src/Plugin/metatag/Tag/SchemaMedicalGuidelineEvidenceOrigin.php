<?php

namespace Drupal\schema_medical_guideline\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'evidenceOrigin' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_guideline_evidence_origin",
 *   label = @Translation("evidenceOrigin"),
 *   description = @Translation("Source of the data used to formulate the guidance, e.g. RCT, consensus opinion, etc."),
 *   name = "evidenceOrigin",
 *   group = "schema_medical_guideline",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalGuidelineEvidenceOrigin extends SchemaNameBase {

}
