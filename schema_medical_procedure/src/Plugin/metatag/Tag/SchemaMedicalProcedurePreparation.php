<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'preparation' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_preparation",
 *   label = @Translation("preparation"),
 *   description = @Translation("Typical preparation that a patient must undergo before having the procedure performed."),
 *   name = "preparation",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedurePreparation extends SchemaNameBase {

}
