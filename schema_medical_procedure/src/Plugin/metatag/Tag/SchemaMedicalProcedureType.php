<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical procedure."),
 *   name = "@type",
 *   group = "schema_medical_procedure",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalProcedure",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalProcedureType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalProcedure',
      '- DiagnosticProcedure',
      '- PalliativeProcedure',
      '- PhysicalExam',
      '- SurgicalProcedure',
      '- TherapeuticProcedure',
    ];
  }

}
