<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'followup' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_followup",
 *   label = @Translation("followup"),
 *   description = @Translation("Typical or recommended followup care after the procedure is performed."),
 *   name = "followup",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedureFollowup extends SchemaNameBase {

}
