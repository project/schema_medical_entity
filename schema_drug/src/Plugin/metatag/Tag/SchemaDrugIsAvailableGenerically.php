<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'isAvailableGenerically' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_is_available_generically",
 *   label = @Translation("isAvailableGenerically"),
 *   description = @Translation(""),
 *   name = "isAvailableGenerically",
 *   group = "schema_drug",
 *   weight = 150,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugIsAvailableGenerically extends SchemaNameBase {

}
