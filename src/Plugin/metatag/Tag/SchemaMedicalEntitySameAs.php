<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'sameAs' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_same_as",
 *   label = @Translation("sameAs"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website."),
 *   name = "sameAs",
 *   group = "schema_medical_entity",
 *   weight = 3,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class SchemaMedicalEntitySameAs extends SchemaNameBase {

}
