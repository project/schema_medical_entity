<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;

/**
 * Schema.org SupersededBy trait.
 */
trait SchemaSupersededByTrait {

  /**
   * Form keys.
   */
  public static function supersededByFormKeys() {
    return [
      '@type',
      'supersededBy',
    ];
  }

  /**
   * The form element.
   */
  public function supersededByForm($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $selector = ':input[name="' . $input_values['visibility_selector'] . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [
      $visibility['invisible'],
      $visibility2['invisible'],
    ];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    // Add a pivot option to the form.
    $form['pivot'] = (is_array($value) ? $this->pivotItem($value) : []);
    $form['pivot']['#states'] = $visibility;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'Class' => $this->t('Class'),
        'Enumeration' => $this->t('Enumeration'),
        'Property' => $this->t('Property'),
      ],
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $form['supersededBy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('supersededBy'),
      '#description' => $this->t("Relates a term (i.e. a property, class or enumeration) to one that supersedes it."),
      '#default_value' => !empty($value['supersededBy']) ? $value['supersededBy'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $keys = static::supersededByFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key]['#states'] = $visibility;
      }
    }

    return $form;
  }

}
