<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'pregnancyWarning' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_pregnancy_warning",
 *   label = @Translation("pregnancyWarning"),
 *   description = @Translation(""),
 *   name = "pregnancyWarning",
 *   group = "schema_drug",
 *   weight = 250,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugPregnancyWarning extends SchemaNameBase {

}
