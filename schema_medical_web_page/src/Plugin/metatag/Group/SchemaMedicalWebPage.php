<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Group;

use Drupal\schema_metatag\Plugin\metatag\Group\SchemaGroupBase;

/**
 * Provides a plugin for the 'MedicalWebPage' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_web_page",
 *   label = @Translation("Schema.org: MedicalWebPage"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalWebPage",
 *   }),
 *   weight = 10,
 * )
 */
class SchemaMedicalWebPage extends SchemaGroupBase {

}
