<?php

namespace Drupal\schema_anatomical_system\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_system_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of anatomical system."),
 *   name = "@type",
 *   group = "schema_anatomical_system",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "AnatomicalSystem",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaAnatomicalSystemType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'AnatomicalSystem',
    ];
  }

}
