<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'recognizingAuthority' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_recognizing_authority",
 *   label = @Translation("recognizingAuthority"),
 *   description = @Translation("If applicable, the organization that officially recognizes this entity as part of its endorsed system of medicine."),
 *   name = "recognizingAuthority",
 *   group = "schema_medical_entity",
 *   weight = 14,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "organization",
 *   tree_parent = {
 *     "Person",
 *     "Organization",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaMedicalEntityRecognizingAuthority extends SchemaNameBase {

}
