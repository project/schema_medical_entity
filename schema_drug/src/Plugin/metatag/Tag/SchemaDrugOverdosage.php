<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'overdosage' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_overdosage",
 *   label = @Translation("overdosage"),
 *   description = @Translation(""),
 *   name = "overdosage",
 *   group = "schema_drug",
 *   weight = 230,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugOverdosage extends SchemaNameBase {

}
