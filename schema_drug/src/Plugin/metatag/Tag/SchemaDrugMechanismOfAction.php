<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'mechanismOfAction' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_mechanism_of_action",
 *   label = @Translation("mechanismOfAction"),
 *   description = @Translation(""),
 *   name = "mechanismOfAction",
 *   group = "schema_drug",
 *   weight = 210,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugMechanismOfAction extends SchemaNameBase {

}
