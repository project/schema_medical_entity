<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'preOp' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_pre_op",
 *   label = @Translation("preOp"),
 *   description = @Translation("A description of the workup, testing, and other preparations required before implanting this device."),
 *   name = "preOp",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDevicePreOp extends SchemaNameBase {

}
