<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'postOp' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_device_post_op",
 *   label = @Translation("postOp"),
 *   description = @Translation("A description of the postoperative procedures, care, and/or followups for this device."),
 *   name = "postOp",
 *   group = "schema_medical_device",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalDevicePostOp extends SchemaNameBase {

}
