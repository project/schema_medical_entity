<?php

namespace Drupal\schema_medical_web_page\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'medicalAudience' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_web_page_medical_audience",
 *   label = @Translation("medicalAudience"),
 *   description = @Translation("Target audiences for medical web pages."),
 *   name = "medicalAudience",
 *   group = "schema_medical_web_page",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "text",
 *   tree_parent = {},
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalWebPageMedicalAudience extends SchemaNameBase {

}
