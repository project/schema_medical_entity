<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_medical_entity\Plugin\metatag\Base\SchemaMedicineSystemBase;

/**
 * Provides a plugin for the 'medicineSystem' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_medicine_system",
 *   label = @Translation("medicineSystem"),
 *   description = @Translation("The system of medicine that includes this MedicalEntity, for example 'evidence-based', 'homeopathic', 'chiropractic', etc."),
 *   name = "medicineSystem",
 *   group = "schema_medical_entity",
 *   weight = 13,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityMedicineSystem extends SchemaMedicineSystemBase {

}
