<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'diagram' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_diagram",
 *   label = @Translation("diagram"),
 *   description = @Translation("An image containing a diagram that illustrates the structure and/or its component substructures and/or connections with other structures."),
 *   name = "diagram",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "image",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "image_object",
 *   tree_parent = {
 *     "ImageObject",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaAnatomicalStructureDiagram extends SchemaNameBase {

}
