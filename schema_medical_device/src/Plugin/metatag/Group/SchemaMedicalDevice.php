<?php

namespace Drupal\schema_medical_device\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'MedicalDevice' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_medical_device",
 *   label = @Translation("Schema.org: MedicalDevice"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/MedicalDevice",
 *   }),
 *   weight = 1060,
 * )
 */
class SchemaMedicalDevice extends SchemaMedicalEntity {

}
