<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'administrationRoute' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_administration_route",
 *   label = @Translation("administrationRoute"),
 *   description = @Translation(""),
 *   name = "administrationRoute",
 *   group = "schema_drug",
 *   weight = 30,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugAdministrationRoute extends SchemaNameBase {

}
