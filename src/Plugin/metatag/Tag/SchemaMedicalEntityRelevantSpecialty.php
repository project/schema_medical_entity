<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_medical_entity\Plugin\metatag\Base\SchemaMedicalSpecialtyBase;

/**
 * Provides a plugin for the 'relevantSpecialty' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_relevant_specialty",
 *   label = @Translation("relevantSpecialty"),
 *   description = @Translation("If applicable, a medical specialty in which this entity is relevant."),
 *   name = "relevantSpecialty",
 *   group = "schema_medical_entity",
 *   weight = 15,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityRelevantSpecialty extends SchemaMedicalSpecialtyBase {

}
