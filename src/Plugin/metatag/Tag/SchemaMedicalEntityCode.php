<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_medical_entity\Plugin\metatag\Base\SchemaMedicalCodeBase;

/**
 * Provides a plugin for the 'code' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_code",
 *   label = @Translation("code"),
 *   description = @Translation("A medical code for the entity, taken from a controlled vocabulary or ontology such as ICD-9, DiseasesDB, MeSH, SNOMED-CT, RxNorm, etc."),
 *   name = "code",
 *   group = "schema_medical_entity",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityCode extends SchemaMedicalCodeBase {

}
