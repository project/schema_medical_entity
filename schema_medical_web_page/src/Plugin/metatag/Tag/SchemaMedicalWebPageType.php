<?php

namespace Drupal\schema_medical_web_page\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_web_page_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical web page."),
 *   name = "@type",
 *   group = "schema_medical_web_page",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalWebPage",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaMedicalWebPageType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalWebPage',
    ];
  }

}
