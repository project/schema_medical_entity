<?php

namespace Drupal\schema_medical_guideline\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'evidenceLevel' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_guideline_evidence_level",
 *   label = @Translation("evidenceLevel"),
 *   description = @Translation("Strength of evidence of the data used to formulate the guideline (enumerated)."),
 *   name = "evidenceLevel",
 *   group = "schema_medical_guideline",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalGuidelineEvidenceLevel extends SchemaNameBase {

}
