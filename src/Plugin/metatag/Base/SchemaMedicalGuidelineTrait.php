<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Base;

use Drupal\schema_metatag\SchemaMetatagManager;

/**
 * Schema.org MedicalGuideline trait.
 */
trait SchemaMedicalGuidelineTrait {

  /**
   * Form keys.
   */
  public static function medicalGuidelineFormKeys() {
    return [
      '@type',
      'evidenceLevel',
      'evidenceOrigin',
      'guidelineDate',
      'guidelineSubject',
    ];
  }

  /**
   * The form element.
   */
  public function medicalGuidelineForm($input_values) {

    $input_values += SchemaMetatagManager::defaultInputValues();
    $value = $input_values['value'];

    // Get the id for the nested @type element.
    $selector = ':input[name="' . $input_values['visibility_selector'] . '[@type]"]';
    $visibility = ['invisible' => [$selector => ['value' => '']]];
    $selector2 = SchemaMetatagManager::altSelector($selector);
    $visibility2 = ['invisible' => [$selector2 => ['value' => '']]];
    $visibility['invisible'] = [
      $visibility['invisible'],
      $visibility2['invisible'],
    ];

    $form['#type'] = 'fieldset';
    $form['#title'] = $input_values['title'];
    $form['#description'] = $input_values['description'];
    $form['#tree'] = TRUE;

    // Add a pivot option to the form.
    $form['pivot'] = (is_array($value) ? $this->pivotItem($value) : []);
    $form['pivot']['#states'] = $visibility;

    $form['@type'] = [
      '#type' => 'select',
      '#title' => $this->t('@type'),
      '#default_value' => !empty($value['@type']) ? $value['@type'] : '',
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#options' => [
        'MedicalGuideline' => $this->t('MedicalGuideline'),
      ],
      '#required' => $input_values['#required'],
      '#weight' => -10,
    ];

    $form['evidenceLevel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('evidenceLevel'),
      '#description' => $this->t("Strength of evidence of the data used to formulate the guideline (enumerated)."),
      '#default_value' => !empty($value['evidenceLevel']) ? $value['evidenceLevel'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['evidenceOrigin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('evidenceOrigin'),
      '#description' => $this->t("Source of the data used to formulate the guidance, e.g. RCT, consensus opinion, etc."),
      '#default_value' => !empty($value['evidenceOrigin']) ? $value['evidenceOrigin'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['guidelineDate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('guidelineDate'),
      '#description' => $this->t("Date on which this guideline's recommendation was made."),
      '#default_value' => !empty($value['guidelineDate']) ? $value['guidelineDate'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $form['guidelineSubject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('guidelineSubject'),
      '#description' => $this->t("The medical conditions, treatments, etc. that are the subject of the guideline."),
      '#default_value' => !empty($value['guidelineSubject']) ? $value['guidelineSubject'] : '',
      '#maxlength' => 255,
      '#required' => $input_values['#required'],
    ];

    $keys = static::medicalGuidelineFormKeys();
    foreach ($keys as $key) {
      if ($key != '@type') {
        $form[$key]['#states'] = $visibility;
      }
    }

    return $form;
  }

}
