<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'image' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_image",
 *   label = @Translation("image"),
 *   description = @Translation("RECOMMENDED BY GOOGLE. The primary image for this item."),
 *   name = "image",
 *   group = "schema_medical_entity",
 *   weight = 5,
 *   type = "image",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "image_object",
 *   tree_parent = {
 *     "ImageObject",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaMedicalEntityImage extends SchemaNameBase {

}
