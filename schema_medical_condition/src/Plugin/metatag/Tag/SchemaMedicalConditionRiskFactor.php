<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'riskFactor' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_risk_factor",
 *   label = @Translation("riskFactor"),
 *   description = @Translation("A modifiable or non-modifiable factor that increases the risk of a patient contracting this condition, e.g. age, coexisting condition."),
 *   name = "riskFactor",
 *   group = "schema_medical_condition",
 *   weight = 120,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionRiskFactor extends SchemaNameBase {

}
