<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'secondaryPrevention' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_secondary_prevention",
 *   label = @Translation("secondaryPrevention"),
 *   description = @Translation("A preventative therapy used to prevent reoccurrence of the medical condition after an initial episode of the condition."),
 *   name = "secondaryPrevention",
 *   group = "schema_medical_condition",
 *   weight = 130,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionSecondaryPrevention extends SchemaNameBase {

}
