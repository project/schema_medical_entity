<?php

namespace Drupal\schema_medical_procedure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'howPerformed' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_procedure_how_performed",
 *   label = @Translation("howPerformed"),
 *   description = @Translation("How the procedure is performed."),
 *   name = "howPerformed",
 *   group = "schema_medical_procedure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalProcedureHowPerformed extends SchemaNameBase {

}
