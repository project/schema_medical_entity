<?php

namespace Drupal\schema_anatomical_structure\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'function' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_anatomical_structure_function",
 *   label = @Translation("function"),
 *   description = @Translation("Function of the anatomical structure."),
 *   name = "function",
 *   group = "schema_anatomical_structure",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaAnatomicalStructureFunction extends SchemaNameBase {

}
