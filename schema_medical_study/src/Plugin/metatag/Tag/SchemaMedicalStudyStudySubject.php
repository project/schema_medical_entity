<?php

namespace Drupal\schema_medical_study\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'studySubject' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_study_study_subject",
 *   label = @Translation("studySubject"),
 *   description = @Translation("A subject of the study, i.e. one of the medical conditions, therapies, devices, drugs, etc. investigated by the study."),
 *   name = "studySubject",
 *   group = "schema_medical_study",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalStudyStudySubject extends SchemaNameBase {

}
