<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'signOrSymptom' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_sign_or_symptom",
 *   label = @Translation("signOrSymptom"),
 *   description = @Translation("A sign or symptom of this condition. Signs are objective or physically observable manifestations of the medical condition while symptoms are the subjective experience of the medical condition."),
 *   name = "signOrSymptom",
 *   group = "schema_medical_condition",
 *   weight = 140,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionSignOrSymptom extends SchemaNameBase {

}
