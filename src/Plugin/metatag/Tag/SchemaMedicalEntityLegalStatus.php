<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'legalStatus' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_legal_status",
 *   label = @Translation("legalStatus"),
 *   description = @Translation("The drug or supplement's legal status, including any controlled substance schedules that apply."),
 *   name = "legalStatus",
 *   group = "schema_medical_entity",
 *   weight = 12,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalEntityLegalStatus extends SchemaNameBase {

}
