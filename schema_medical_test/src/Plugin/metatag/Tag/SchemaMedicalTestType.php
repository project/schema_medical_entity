<?php

namespace Drupal\schema_medical_test\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'type' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_test_type",
 *   label = @Translation("@type"),
 *   description = @Translation("REQUIRED. The type of medical test."),
 *   name = "@type",
 *   group = "schema_medical_test",
 *   weight = -10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 *   property_type = "type",
 *   tree_parent = {
 *     "MedicalTest",
 *   },
 *   tree_depth = -1,
 * )
 */
class SchemaMedicalTestType extends SchemaNameBase {

  /**
   * {@inheritdoc}
   */
  public static function labels() {
    return [
      'MedicalTest',
      '- BloodTest',
      '- ImagingTest',
      '- MedicalTestPanel',
      '- PathologyTest',
    ];
  }

}
