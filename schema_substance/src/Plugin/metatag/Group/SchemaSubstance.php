<?php

namespace Drupal\schema_substance\Plugin\metatag\Group;

use Drupal\schema_medical_entity\Plugin\metatag\Group\SchemaMedicalEntity;

/**
 * Provides a plugin for the 'Substance' meta tag group.
 *
 * @MetatagGroup(
 *   id = "schema_substance",
 *   label = @Translation("Schema.org: Substance"),
 *   description = @Translation("See Schema.org definitions for this Schema type at <a href="":url"">:url</a>.", arguments = {
 *     ":url" = "https://schema.org/Substance",
 *   }),
 *   weight = 2030,
 * )
 */
class SchemaSubstance extends SchemaMedicalEntity {

}
