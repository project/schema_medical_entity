<?php

namespace Drupal\schema_physician\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'schema_physician_member_of' meta tag.
 *
 * - 'id' should be a globally unique id.
 * - 'name' should match the Schema.org element name.
 * - 'group' should match the id of the group that defines the Schema.org type.
 *
 * @MetatagTag(
 *   id = "schema_physician_member_of",
 *   label = @Translation("memberOf"),
 *   description = @Translation("An Organization (or ProgramMembership) to which this Person or Organization belongs."),
 *   name = "memberOf",
 *   group = "schema_physician",
 *   weight = 2,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "memberOf",
 *   tree_parent = {
 *     "Organization",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaPhysicianMemberOf extends SchemaNameBase {

}
