<?php

namespace Drupal\schema_drug_cost\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'costCurrency' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_cost_cost_currency",
 *   label = @Translation("costCurrency"),
 *   description = @Translation(""),
 *   name = "costCurrency",
 *   group = "schema_drug_cost",
 *   weight = 40,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugCostCostCurrency extends SchemaNameBase {

}
