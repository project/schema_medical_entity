<?php

namespace Drupal\schema_medical_condition\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'cause' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_condition_cause",
 *   label = @Translation("cause"),
 *   description = @Translation("Specifying a cause of something in general. e.g in medicine , one of the causative agent(s) that are most directly responsible for the pathophysiologic process that eventually results in the occurrence."),
 *   name = "cause",
 *   group = "schema_medical_condition",
 *   weight = 20,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaMedicalConditionCause extends SchemaNameBase {

}
