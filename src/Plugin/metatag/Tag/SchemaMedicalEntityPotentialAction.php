<?php

namespace Drupal\schema_medical_entity\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'potentialAction' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_medical_entity_potential_action",
 *   label = @Translation("potentialAction"),
 *   description = @Translation("Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role."),
 *   name = "potentialAction",
 *   group = "schema_medical_entity",
 *   weight = 7,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = TRUE,
 *   property_type = "action",
 *   tree_parent = {
 *     "ReadAction",
 *   },
 *   tree_depth = 0,
 * )
 */
class SchemaMedicalEntityPotentialAction extends SchemaNameBase {
}
