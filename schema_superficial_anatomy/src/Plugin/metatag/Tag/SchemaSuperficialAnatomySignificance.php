<?php

namespace Drupal\schema_superficial_anatomy\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'significance' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_superficial_anatomy_significance",
 *   label = @Translation("significance"),
 *   description = @Translation("The significance associated with the superficial anatomy; as an example, how characteristics of the superficial anatomy can suggest underlying medical conditions or courses of treatment."),
 *   name = "significance",
 *   group = "schema_superficial_anatomy",
 *   weight = 10,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE
 * )
 */
class SchemaSuperficialAnatomySignificance extends SchemaNameBase {

}
