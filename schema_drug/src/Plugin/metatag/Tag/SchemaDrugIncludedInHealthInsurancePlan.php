<?php

namespace Drupal\schema_drug\Plugin\metatag\Tag;

use Drupal\schema_metatag\Plugin\metatag\Tag\SchemaNameBase;

/**
 * Provides a plugin for the 'includedInHealthInsurancePlan' meta tag.
 *
 * @MetatagTag(
 *   id = "schema_drug_included_in_health_insurance_plan",
 *   label = @Translation("includedInHealthInsurancePlan"),
 *   description = @Translation(""),
 *   name = "includedInHealthInsurancePlan",
 *   group = "schema_drug",
 *   weight = 130,
 *   type = "string",
 *   secure = FALSE,
 *   multiple = FALSE,
 * )
 */
class SchemaDrugIncludedInHealthInsurancePlan extends SchemaNameBase {

}
